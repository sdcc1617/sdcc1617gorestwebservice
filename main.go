package main

import (
    "log"
    "net/http"
    "github.com/gorilla/handlers"
    "os"
    "fmt"
    "github.com/aws/aws-sdk-go/service/dynamodb"
    "github.com/aws/aws-sdk-go/aws/session"
    "github.com/aws/aws-sdk-go/aws"
    "github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

/**
 * Main principale del microservizio RESTFUL scritto in Go per il progetto di SDCC
 * 
 * Il microservizio è in esecuzione su una porta, configurabile nel file config.go
 * 
 */

func main() {
	elbEtcd = os.Args[1]
	cloudFrontAddress = os.Args[2]
	region = os.Args[3]
	bucketS3 = os.Args[4]
	dynamoDBTable = os.Args[5]
	dynamoDBTableRepository = os.Args[6]
    directory = "dir"//os.Args[7]
    etcdDomanin = "http://"+ elbEtcd +":2379/v2/"
	etcdDomaninKeys = etcdDomanin+ "keys/"
	cloudFront = "http://"+cloudFrontAddress+"/images/"
    fmt.Println(elbEtcd)
    fmt.Println(etcdDomaninKeys)
    fmt.Println(cloudFrontAddress)
    fmt.Println(region)
    fmt.Println(bucketS3)
    fmt.Println(dynamoDBTable)
    fmt.Println(dynamoDBTableRepository)
    fmt.Println(directory)
    sessionClient = session.New(&aws.Config{Region: aws.String(region)})
	svcDynamoDB = dynamodb.New(sessionClient)
	// Create S3 service client
	svcS3 = s3.New(sessionClient)
	sess = session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))
	uploaderS3 = s3manager.NewUploader(sess)
    router := NewRouter()
    log.Fatal(http.ListenAndServe(port, handlers.CORS()(router)))

}

