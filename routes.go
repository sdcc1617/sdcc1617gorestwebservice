package main

import "net/http"

type Route struct {
    Name        string
    Method      string
    Pattern     string
    HandlerFunc http.HandlerFunc
}

type Routes []Route

/**
 * Endoint dell'applicazione, con i relativi metodi che vengono invocati
 *
 */

var routes = Routes{
    Route{
        "ReceiveUserSignalation",
        "POST",
        "/receiveUserSignalation",
        ReceiveUserSignalation,
    },
    Route{
        "GetEtcdMembers",
        "GET",
        "/getEtcdMembers",
        GetEtcdMembers,
    },

    Route{
        "GetEtcdAllObjects",
        "GET",
        "/getEtcdAllObjects",
        GetEtcdAllObjects,
    },
     Route{
        "GetEtcdObjectFromKey",
        "GET",
        "/getEtcdObjectFromKey/{dir}/{keyId}",
        GetEtcdObjectFromKey,
    },
    Route{
        "SetEtcdObjectFromKey",
        "PUT",
        "/setEtcdObjectFromKey/{dir}/{keyId}/{value}",
        SetEtcdObjectFromKey,
    },
    Route{
        "CreateEtcdObjectFromKey",
        "POST",
        "/createEtcdObjectFromKey/{dir}/{keyId}/{value}",
        CreateEtcdObjectFromKey,
    },
    Route{
        "DeleteEtcdObjectFromKey",
        "DELETE",
        "/deleteEtcdObjectFromKey/{dir}/{keyId}",
        DeleteEtcdObjectFromKey,
    },
   
    Route{
        "GetItemsFromSignalationsCollection",
        "GET",
        "/getItemsFromSignalationsCollection/{longsx}/{longdx}/{latsx}/{latdx}",
         GetItemsFromSignalationsCollection,
    },
    Route{
        "GetSignalationsFromEtcd",
        "GET",
        "/getSignalationsFromEtcd/{dir}",
         GetSignalationsFromEtcd,
    },
    Route{
        "GetSignalationFromDynamoDB",
        "GET",
        "/getSignalationFromDynamoDB/{long}/{lat}",
         GetSignalationFromDynamoDB,
    },
    Route{
        "ReassignSignalationInEtcd",
        "POST",
        "/reassignSignalationInEtcd/{dir}/{newdir}/{stateId}",
         ReassignSignalationInEtcd,
    },
    Route{
        "GetAllPossibleSignalationStates",
        "GET",
        "/getAllPossibleSignalationStates",
         GetAllPossibleSignalationStates,
    },
    Route{
        "ChangeSignalationState",
        "POST",
        "/changeSignalationState/{state}/{dir}/{stateId}",
         ChangeSignalationState,
    },
    Route{
        "CompletedSignalation",
        "POST",
        "/completedSignalation/{long}/{lat}/{dir}/{keyId}",
         CompletedSignalation,
    },

}