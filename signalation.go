package main

type Signalation struct {
	Id        int       `json:"id"`
    Topic     string    `json:"topic"`
    Title     string    `json:"title"`
    Message   string    `json:"message"`
    Longitude string    `json:"longitude"`
    Latitude  string    `json:"latitude"`
    Image	  string	`json:"url"`
    Due       string 	`json:"due"`
    StateId   string 	`json:"stateId"`
    Owner     string 	`json:"owner"`
    By   	  string 	`json:"by"`
}

type Signalations []Signalation