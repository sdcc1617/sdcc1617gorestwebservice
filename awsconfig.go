package main

import (
 "github.com/aws/aws-sdk-go/service/s3/s3manager"
 "github.com/aws/aws-sdk-go/service/dynamodb"
 "github.com/aws/aws-sdk-go/service/s3"
 "github.com/aws/aws-sdk-go/aws/session")
/** 
 * File di inizializzazione dei servizi di AWS
 *
 */

var sessionClient *session.Session
var svcDynamoDB *dynamodb.DynamoDB
var	svcS3 *s3.S3
var sess *session.Session
var uploaderS3 *s3manager.Uploader

	