package main

type EtcdAction struct {
    Action    string  `json:"action"`
    Node      Node    `json:"node"`
}

type Node struct {
	Dir bool 		`json:"dir"`
	Nodes Nodes 	`json:"nodes"`
}


type Nodes []Object

type Object struct{
	Key    			string  `json:"key"`
	Value    		string  `json:"value"`
	Nodes 			Nodes  	`json:"nodes"`
	ModifiedIndex   int  	`json:"modifiedIndex"`
	CreatedIndex    int  	`json:"createdIndex"`
}