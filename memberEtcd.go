package main

type Member struct {
    Id        string     `json:"id"`
    Name      string    `json:"name"`
    PeerURLs []string   `json:"peerURLs"`
    ClientURLs []string  `json:"clientURLs"`
}

type Members []Member

type MemberList struct{

	Members Members `json:"members"`
}