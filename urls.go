package main

/**
 * Stringhe statiche dell'applicazione
 */
var elbEtcd string //= "127.0.0.1" 
var etcdDomanin string //= "http://"+ elbEtcd +":2379/v2/"
var etcdDomaninKeys string //= etcdDomanin+ "keys/"
var bucketS3 string //= "sdcc1617utilityapp/"
var cloudFrontAddress string
var cloudFront string //= "http://"+cloudFrontAddress+"/" // "http://d1egz9u6mhouz8.cloudfront.net/"
var dynamoDBTable string //= "Sdcc1617SignalationsTable2"
var dynamoDBTableRepository string //= "Sdcc1617SignalationsTableRepository"
var region string  //="eu-west-1"
var port string = ":8082"
var directory string //= "unassigned"
