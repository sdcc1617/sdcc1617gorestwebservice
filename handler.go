package main

import (
    "encoding/json"
    "fmt"
    "net/http"
    "io"
    "io/ioutil"
    "github.com/gorilla/mux"
    "github.com/aws/aws-sdk-go/service/dynamodb"
    "github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
    "github.com/aws/aws-sdk-go/aws"
    "github.com/aws/aws-sdk-go/service/s3/s3manager"
    "bytes"
    "strings"
    "log"
    "time"
    "mime"
    "mime/multipart"
)

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Interazione con Client Android
 *
 */
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /**
     * Endpoint per un utente Android che invia una segnalazione.  
     * 
     */

    func ReceiveUserSignalation(w http.ResponseWriter, r *http.Request){
        var img []byte
        var topic string
        var title string 
        var message string
        var longitude string
        var latitude string
        var by string

        body:= io.LimitReader(r.Body, 1048576)
        /*body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
         if err != nil {
            panic(err)
        }
        log.Printf("%s\n",  body)*/
       mediaType, params, err := mime.ParseMediaType(r.Header.Get("Content-Type"))
        if err != nil {
            panic(err)
        }
        if strings.HasPrefix(mediaType, "multipart/") {
            mr := multipart.NewReader(body, params["boundary"])

            i := 0;
            for {

                p, err := mr.NextPart()
                if err == io.EOF {
                    break
                }

                if err != nil {
                    panic(err)
                }
                slurp, err := ioutil.ReadAll(p)
                if err != nil {
                    panic(err)
                }

                if i==0 {
                    img = slurp
                } else if i==1 {
                    topic = string(slurp)
                } else if i==2 {
                    title = string(slurp)
                } else if i==3 {
                    message = string(slurp)
                } else if i==4 {
                    longitude = string(slurp)
                } else if i==5 {
                    latitude = string(slurp)
                } else if i==6 {
                    by = string(slurp)
                }
                i++;
            }
        }

        var signalation Signalation
        hashOffset := RandNumberRunes(5);
        signalation.Topic = topic
        signalation.Title = title
        signalation.Message = message
        signalation.Longitude = longitude + hashOffset 
        signalation.Latitude = latitude + hashOffset 
        signalation.By = by
        
        if(len(img) > 5){
            image := bytes.NewReader(img)
            filename := RandStringRunes(128);
            _, err := uploaderS3.Upload(&s3manager.UploadInput{
                Bucket: aws.String(bucketS3),
                Key: aws.String("/images/"+filename+".jpg"),
                Body: image,
                ACL: aws.String("public-read"),
            })
            if err != nil {
                // Print the error and exit.
               panic(err)
           }
            signalation.Image = cloudFront +filename+".jpg"
        } else {
            signalation.Image = "null"
        }
        
        signalation.Due = time.Now().Format(time.RFC850)
        //t := RepoCreateSignalation(signalation)
 
        //parlare con ETCD (Non va bene che sia una post!)
        var etcdCreate EtcdCreate
        //dir := directory //per ora
        keyId := RandLetterNumberRunes(128);
        value := "TODO" + "|" +  signalation.Longitude + "|" + signalation.Latitude ;
        /*resp, err := http.Post(etcdDomaninKeys +dir + "/" + keyId+"?value="+value, "application/json; charset=UTF-8", nil)
        if err != nil {
            // handle error
            panic(err)
        }*/
        // Per ora è una post, ma dovrebbe essere una put
        req, err := http.NewRequest("PUT", etcdDomaninKeys+directory+"/" + keyId+"?value="+value, nil)
        if err != nil {
            panic(err)
        }
        client := &http.Client{}
        resp, err := client.Do(req)
        if err != nil {
            panic(err)
        }
        body2, err := ioutil.ReadAll(resp.Body)
          
        if err := json.Unmarshal(body2, &etcdCreate); err != nil {
            w.Header().Set("Content-Type", "application/json; charset=UTF-8")
            w.WriteHeader(422) // unprocessable entity
            if err := json.NewEncoder(w).Encode(err); err != nil {
                panic(err)
            }
        }
        signalation.StateId = keyId;

        
        //parlare con DynamoDB
        
        result, err := svcDynamoDB.PutItem(&dynamodb.PutItemInput{
            TableName: aws.String(dynamoDBTable), 
            Item: map[string]*dynamodb.AttributeValue{ 
                "longitude": {
                        N: aws.String(signalation.Longitude),
                },
                "latitude":{
                        N: aws.String(signalation.Latitude),
                },
                "topic":{
                        S: aws.String(signalation.Topic),
                },
                "title":{
                        S: aws.String(signalation.Title),
                },
                "message":{
                        S: aws.String(signalation.Message),
                },
                "image":{
                        S: aws.String(signalation.Image),
                },
                "created":{
                        S: aws.String(signalation.Due),
                },
                "stateId":{
                        S: aws.String(signalation.StateId),
                },
                "by":{
                        S: aws.String(signalation.By),
                },
                "owner":{
                        S: aws.String(directory),
                },
            },
        })

        if(err!=nil){
            panic(err)
        }
        fmt.Fprintln(w, result)

        if err := json.NewEncoder(w).Encode(signalation); err != nil {
            panic(err)
        }
        w.Header().Set("Content-Type", "application/json; charset=UTF-8")
        w.WriteHeader(http.StatusOK)
    }

     /**  
     * Funzione che è esposta tramite servizio Rest, e consente ad un utente di accedere all'elenco delle segnalazioni
     * in un rettangolo specificato
     */

    func GetItemsFromSignalationsCollection(w http.ResponseWriter, r *http.Request){
        vars := mux.Vars(r)
        longsx := vars["longsx"]
        longdx := vars["longdx"]
        latsx := vars["latsx"]
        latdx := vars["latdx"]
        if(longsx < longdx){
                params := &dynamodb.ScanInput{
                    TableName: aws.String(dynamoDBTable), // Required
                    ConsistentRead:      aws.Bool(false),
                    ExpressionAttributeNames: map[string]*string{
                    "#long": aws.String("longitude"), // Required
                    // More values...
                    "#lat": aws.String("latitude"), // Required
                    },
                    ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
                        ":longsx": {
                            N: aws.String(longsx), // Required   
                            },
                        ":longdx": {
                            N: aws.String(longdx), // Required
                            },
                        ":latsx": {
                            N: aws.String(latsx), // Required 
                            },   
                        ":latdx": {
                            N: aws.String(latdx), // Required
                            },
                    },
                    FilterExpression:  aws.String("#long BETWEEN "+ ":longsx " +"AND "+ ":longdx "+
                        "AND #lat BETWEEN "+":latsx " +"AND "+ ":latdx"),
                }
                resp, err := svcDynamoDB.Scan(params)
                
                if err != nil {
                    // Print the error, cast err to awserr.Error to get the Code and
                    // Message from an error.
                    panic(err)
                }
            
            // Convert response data 
            // back to Go type 
            s := SignalationsDb{}
            signalationsToAndroid := SignalationsToAndroid{}
            var signalationToAndroid SignalationToAndroid;
            //s := SignalationScans{}
            dynamodbattribute.UnmarshalListOfMaps(resp.Items, &s)
            for i, signalationDb := range s {
                i++;
                signalationToAndroid.Topic = signalationDb.Topic;
                signalationToAndroid.Title = signalationDb.Title;
                signalationToAndroid.Longitude = signalationDb.Longitude;
                signalationToAndroid.Latitude = signalationDb.Latitude;
                signalationToAndroid.Message = signalationDb.Message;
                signalationToAndroid.Image = signalationDb.Image;
                signalationToAndroid.Due = signalationDb.Due;
                signalationToAndroid.StateId = signalationDb.StateId;
                signalationToAndroid.Owner = signalationDb.Owner;
                signalationToAndroid.By = signalationDb.By;
                signalationsToAndroid = append(signalationsToAndroid, signalationToAndroid)
            }
            w.Header().Set("Content-Type", "application/json; charset=UTF-8")
            w.WriteHeader(http.StatusOK)
            if err := json.NewEncoder(w).Encode(signalationsToAndroid); err != nil {
                panic(err)
            }
            return
        }
        params := &dynamodb.ScanInput{
                    TableName: aws.String(dynamoDBTable), // Required
                    ConsistentRead:      aws.Bool(false),
                    ExpressionAttributeNames: map[string]*string{
                    "#long": aws.String("longitude"), // Required
                    // More values...
                    "#lat": aws.String("latitude"), // Required
                    },
                    ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
                        ":longsx": {
                            N: aws.String(longsx), // Required   
                            },
                        ":longdx": {
                            N: aws.String(longdx), // Required
                            },
                        ":latsx": {
                            N: aws.String(latsx), // Required 
                            },   
                        ":latdx": {
                            N: aws.String(latdx), // Required
                            },
                        ":exsx": {
                            N: aws.String("180.0"),
                            },
                        ":exdx": {
                            N: aws.String("-180.0"),
                            },
                    },  
                    FilterExpression:  aws.String("#long BETWEEN "+ ":longsx " +"AND "+ ":exsx "+
                        "AND #long BETWEEN "+ ":exdx " +"AND "+ ":longdx "+
                        "AND #lat BETWEEN "+":latsx " +"AND "+ ":latdx"),
                }
                resp, err := svcDynamoDB.Scan(params)
                
                if err != nil {
                    // Print the error, cast err to awserr.Error to get the Code and
                    // Message from an error.
                    panic(err)
                }
            
            // Convert response data 
            // back to Go type 
            s := SignalationsDb{}
            signalationsToAndroid := SignalationsToAndroid{}
            var signalationToAndroid SignalationToAndroid;
            //s := SignalationScans{}
            dynamodbattribute.UnmarshalListOfMaps(resp.Items, &s)
            for i, signalationDb := range s {
                i++;
                signalationToAndroid.Topic = signalationDb.Topic;
                signalationToAndroid.Title = signalationDb.Title;
                signalationToAndroid.Longitude = signalationDb.Longitude;
                signalationToAndroid.Latitude = signalationDb.Latitude;
                signalationToAndroid.Message = signalationDb.Message;
                signalationToAndroid.Image = signalationDb.Image;
                signalationToAndroid.Due = signalationDb.Due;
                signalationToAndroid.StateId = signalationDb.StateId;
                signalationsToAndroid = append(signalationsToAndroid, signalationToAndroid)
            }
            w.Header().Set("Content-Type", "application/json; charset=UTF-8")
            w.WriteHeader(http.StatusOK)
            if err := json.NewEncoder(w).Encode(signalationsToAndroid); err != nil {
                panic(err)
            }
    }       


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Interazione con il cluster Etcd 
 *
 */
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Funzione che viene invocata per richiedere da Etcd tutti i membri
     */

    func GetEtcdMembers(w http.ResponseWriter, r *http.Request){
        var memberList MemberList
        var members Members
        resp, err := http.Get( etcdDomanin + "members")
        if err != nil {
            // handle error
            panic(err)
        }
        body, err := ioutil.ReadAll(resp.Body)

        if err := json.Unmarshal(body, &memberList); err != nil {
            w.Header().Set("Content-Type", "application/json; charset=UTF-8")
            w.WriteHeader(422) // unprocessable entity
            if err := json.NewEncoder(w).Encode(err); err != nil {
                panic(err)
            }
        }
        members = memberList.Members
        w.Header().Set("Content-Type", "application/json; charset=UTF-8")
        w.WriteHeader(http.StatusOK)
        if err := json.NewEncoder(w).Encode(members); err != nil {
            panic(err)
        }
    }

    /**
     * Funzione che viene invocata per richiedere tutti gli oggetti da etcd 
     */

    func GetEtcdAllObjects(w http.ResponseWriter, r *http.Request){
        var etcdAction EtcdAction;
        resp, err := http.Get( etcdDomaninKeys + "?recursive=true")
        if err != nil {
            // handle error
            panic(err)
        }
        body, err := ioutil.ReadAll(resp.Body)
        if err := json.Unmarshal(body, &etcdAction); err != nil {
            w.Header().Set("Content-Type", "application/json; charset=UTF-8")
            w.WriteHeader(422) // unprocessable entity
            if err := json.NewEncoder(w).Encode(err); err != nil {
                panic(err)
            }
        }
        node := etcdAction.Node
        nodes := node.Nodes
        w.Header().Set("Content-Type", "application/json; charset=UTF-8")
        w.WriteHeader(http.StatusOK)
        if err := json.NewEncoder(w).Encode(nodes); err != nil {
            panic(err)
        }
    }

    /**
     * Funzione che viene invocata per richiedere da etcd un oggetto a partire da una chiave
     */

    func GetEtcdObjectFromKey(w http.ResponseWriter, r *http.Request){
        var etcdGetOneValue EtcdGetOneValue;
    
        vars := mux.Vars(r)
        dir := vars["dir"]
        keyId := vars["keyId"]
        resp, err := http.Get(etcdDomaninKeys +dir + "/" +  keyId)
         if err != nil {
            // handle error
            panic(err)
        }
        body, err := ioutil.ReadAll(resp.Body)
        if err := json.Unmarshal(body, &etcdGetOneValue); err != nil {
            w.Header().Set("Content-Type", "application/json; charset=UTF-8")
            w.WriteHeader(422) // unprocessable entity
            if err := json.NewEncoder(w).Encode(err); err != nil {
                panic(err)
            }
        }
        w.Header().Set("Content-Type", "application/json; charset=UTF-8")
        w.WriteHeader(http.StatusOK)
        if err := json.NewEncoder(w).Encode(etcdGetOneValue); err != nil {
            panic(err)
        }


    }

    /**
     * Funzione che viene invocata per aggiornare in etcd un oggetto a partire da una chiave
     */

    func SetEtcdObjectFromKey(w http.ResponseWriter, r *http.Request){  
        var etcdSet EtcdSet;
        var etcdError EtcdError;
        vars := mux.Vars(r)
        dir := vars["dir"]
        keyId := vars["keyId"]
        log.Printf("%s\n", keyId)
        value := vars["value"]
    
        req, err := http.NewRequest("PUT", etcdDomaninKeys +dir + "/" + keyId+"?value="+value, nil)
        if err != nil {
            panic(err)
        }
        client := &http.Client{}
        resp, err := client.Do(req)
        if err != nil {
            panic(err)
        }
        
        body, err := ioutil.ReadAll(resp.Body)
        if(resp.StatusCode != http.StatusOK && resp.StatusCode != http.StatusCreated) {
            if err := json.Unmarshal(body, &etcdError); err != nil {
                w.Header().Set("Content-Type", "application/json; charset=UTF-8")
                w.WriteHeader(422) // unprocessable entity
                if err := json.NewEncoder(w).Encode(err); err != nil {
                    panic(err)
                }
            } 
            w.Header().Set("Content-Type", "application/json; charset=UTF-8")
            w.WriteHeader(http.StatusForbidden)
            if err := json.NewEncoder(w).Encode(etcdError); err != nil {
                panic(err)
            }
        } else {
            if err := json.Unmarshal(body, &etcdSet); err != nil {
                w.Header().Set("Content-Type", "application/json; charset=UTF-8")
                w.WriteHeader(422) // unprocessable entity
                if err := json.NewEncoder(w).Encode(err); err != nil {
                    panic(err)
                }
            }
            w.Header().Set("Content-Type", "application/json; charset=UTF-8")
            w.WriteHeader(http.StatusOK)
            if err := json.NewEncoder(w).Encode(etcdSet); err != nil {
                panic(err)
            }
        } 
        
    }

   /**
     * Funzione che viene invocata per creare in etcd un oggetto a partire da una chiave
     */

    func CreateEtcdObjectFromKey(w http.ResponseWriter, r *http.Request){
        var etcdCreate EtcdCreate
       
        vars := mux.Vars(r)
        dir := vars["dir"]
        keyId := vars["keyId"]
        value := vars["value"]
        resp, err := http.Post(etcdDomaninKeys +dir + "/" + keyId+"?value="+value, "application/json; charset=UTF-8", nil)
         if err != nil {
            // handle error
            panic(err)
        }
        body, err := ioutil.ReadAll(resp.Body)
          
        if err := json.Unmarshal(body, &etcdCreate); err != nil {
            w.Header().Set("Content-Type", "application/json; charset=UTF-8")
            w.WriteHeader(422) // unprocessable entity
            if err := json.NewEncoder(w).Encode(err); err != nil {
                panic(err)
            }
        }
        w.Header().Set("Content-Type", "application/json; charset=UTF-8")
        w.WriteHeader(http.StatusCreated)
        if err := json.NewEncoder(w).Encode(etcdCreate); err != nil {
            panic(err)
        }
        
    }

    /**
     * Funzione che viene invocata per cancellare da etcd un oggetto a partire da una chiave
     */

    func DeleteEtcdObjectFromKey(w http.ResponseWriter, r *http.Request){
        var etcdSet EtcdSet
        var etcdError EtcdError
        vars := mux.Vars(r)
        dir := vars["dir"]
        keyId := vars["keyId"]

        req, err := http.NewRequest("DELETE", etcdDomaninKeys +dir + "/" + keyId, nil)
        if err != nil {
            log.Fatalln(err)
        }

        client := &http.Client{}
        resp, err := client.Do(req)
        if err != nil {
            log.Fatalln(err)
        }

        body, err := ioutil.ReadAll(resp.Body)

        if(resp.StatusCode != http.StatusOK) {
            if err := json.Unmarshal(body, &etcdError); err != nil {
                w.Header().Set("Content-Type", "application/json; charset=UTF-8")
                w.WriteHeader(422) // unprocessable entity
                if err := json.NewEncoder(w).Encode(err); err != nil {
                    panic(err)
                }
            } 
            w.Header().Set("Content-Type", "application/json; charset=UTF-8")
            w.WriteHeader(http.StatusForbidden)
            if err := json.NewEncoder(w).Encode(etcdError); err != nil {
                panic(err)
            }
        } else {
            if err := json.Unmarshal(body, &etcdSet); err != nil {
                w.Header().Set("Content-Type", "application/json; charset=UTF-8")
                w.WriteHeader(422) // unprocessable entity
                if err := json.NewEncoder(w).Encode(err); err != nil {
                    panic(err)
                }
            }
            w.Header().Set("Content-Type", "application/json; charset=UTF-8")
            w.WriteHeader(http.StatusOK)
            if err := json.NewEncoder(w).Encode(etcdSet); err != nil {
                panic(err)
            }
        } 
    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Interazioni frontend
 *
 */
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Funzione che viene invocata da un operatore web per richiedere ad etcd lo stato delle segnalazioni
     */

    func GetSignalationsFromEtcd(w http.ResponseWriter, r *http.Request){
        var etcdError EtcdError
        var etcdAction EtcdAction;
        vars := mux.Vars(r)
        dir := vars["dir"]

        resp, err := http.Get(etcdDomaninKeys +dir)
        if err != nil {
            // handle error
            panic(err)
        }

        body, err := ioutil.ReadAll(resp.Body)
        if(resp.StatusCode != http.StatusOK) {
            if err := json.Unmarshal(body, &etcdError); err != nil {
                w.Header().Set("Content-Type", "application/json; charset=UTF-8")
                w.WriteHeader(422) // unprocessable entity
                if err := json.NewEncoder(w).Encode(err); err != nil {
                    panic(err)
                }
            } 
            w.Header().Set("Content-Type", "application/json; charset=UTF-8")
            w.WriteHeader(http.StatusForbidden)
            if err := json.NewEncoder(w).Encode(etcdError); err != nil {
                panic(err)
            }
        } else {
            if err := json.Unmarshal(body, &etcdAction); err != nil {
                w.Header().Set("Content-Type", "application/json; charset=UTF-8")
                w.WriteHeader(422) // unprocessable entity
                if err := json.NewEncoder(w).Encode(err); err != nil {
                    panic(err)
                }
            }
            node := etcdAction.Node
            nodes := node.Nodes
            w.Header().Set("Content-Type", "application/json; charset=UTF-8")
            w.WriteHeader(http.StatusOK)
            if err := json.NewEncoder(w).Encode(nodes); err != nil {
                panic(err)
            }

        }
    }
    /**
     * Funzione che viene invocata da un responsabile web per riassegnare la segnalazione ad un operatore web
     */

    func ReassignSignalationInEtcd(w http.ResponseWriter, r *http.Request){
        var etcdGetOneValue EtcdGetOneValue;
        var etcdSet EtcdSet;
        var etcdError EtcdError;
        vars := mux.Vars(r)
        dir := vars["dir"]
        newdir := vars["newdir"]
        stateId := vars["stateId"]
        respGet, err := http.Get(etcdDomaninKeys +dir+"/"+stateId)
        if err != nil {
            // handle error
            panic(err)
        }
        bodyGet, err := ioutil.ReadAll(respGet.Body)
        if err := json.Unmarshal(bodyGet, &etcdGetOneValue); err != nil {
            w.Header().Set("Content-Type", "application/json; charset=UTF-8")
            w.WriteHeader(422) // unprocessable entity
            if err := json.NewEncoder(w).Encode(err); err != nil {
                panic(err)
            }
            return
        }
        value := etcdGetOneValue.NodeGetOneValue.Value

        //delete
        reqDel, err := http.NewRequest("DELETE", etcdDomaninKeys +dir+"/"+stateId, nil)
        if err != nil {
            panic(err)
        }

        client := &http.Client{}
        respDel, err := client.Do(reqDel)
        if err != nil {
            panic(err)
        }

        bodyDel, err := ioutil.ReadAll(respDel.Body)

        if(respDel.StatusCode != http.StatusOK) {
            if err := json.Unmarshal(bodyDel, &etcdError); err != nil {
                w.Header().Set("Content-Type", "application/json; charset=UTF-8")
                w.WriteHeader(422) // unprocessable entity
                if err := json.NewEncoder(w).Encode(err); err != nil {
                    panic(err)
                }
                return
            } 
            w.Header().Set("Content-Type", "application/json; charset=UTF-8")
            w.WriteHeader(http.StatusForbidden)
            if err := json.NewEncoder(w).Encode(etcdError); err != nil {
                panic(err)
            }
            return
        } 


        //N.B qui devo fare una put a DynamoDB per aggiornare il campo di owner
        ss := strings.Split(value, "|")
        var long string
        var lat string
        if(len(ss)>1){
            _, long, lat = ss[0], ss[1], ss[2]
        }else {
            w.Header().Set("Content-Type", "application/json; charset=UTF-8")
            w.WriteHeader(http.StatusForbidden) //<---- viene per questo
            return
        }
        fmt.Println(len(ss))
        input := &dynamodb.GetItemInput{
            Key: map[string]*dynamodb.AttributeValue{
                "longitude": {
                        N: aws.String(long),
                },
                "latitude":{
                        N: aws.String(lat),
                },
            },
            TableName: aws.String(dynamoDBTable),
        }

    
        result, err := svcDynamoDB.GetItem(input)
        if(err!=nil){
            panic(err)
        }
       // fmt.Fprintln(w, result)
        s := SignalationDb{}
        dynamodbattribute.UnmarshalMap(result.Item, &s)
        /*if err := json.NewEncoder(w).Encode(s); err != nil {
            panic(err)
        }*/
        //Faccio la put

        _, err = svcDynamoDB.PutItem(&dynamodb.PutItemInput{
            TableName: aws.String(dynamoDBTable), 
            Item: map[string]*dynamodb.AttributeValue{ 
                "longitude": {
                        N: aws.String(long),
                },
                "latitude":{
                        N: aws.String(lat),
                },
                "topic":{
                        S: aws.String(s.Topic),
                },
                "title":{
                        S: aws.String(s.Title),
                },
                "message":{
                        S: aws.String(s.Message),
                },
                "image":{
                        S: aws.String(s.Image),
                },
                "created":{
                        S: aws.String(s.Due),
                },
                "stateId":{
                        S: aws.String(s.StateId),
                },
                "by":{
                        S: aws.String(s.By),
                },
                "owner":{
                        S: aws.String(newdir),
                },
            },
        })

        if(err!=nil){
            panic(err)
        }

        reqPut, err := http.NewRequest("PUT", etcdDomaninKeys +newdir + "/" + stateId+"?value="+value, nil)
        if err != nil {
            panic(err)
        }
        client = &http.Client{}
        respPut, err := client.Do(reqPut)
        if err != nil {
            panic(err)
        }
        
        bodyPut, err := ioutil.ReadAll(respPut.Body)
        if(respPut.StatusCode != http.StatusOK && respPut.StatusCode != http.StatusCreated) {
            if err := json.Unmarshal(bodyPut, &etcdError); err != nil {
                //dovrei a questo punto, siccome fallisce la put, ma la delete è stata eseguita, ripristinare il valore precedente prima
                //di ritornare..
                w.Header().Set("Content-Type", "application/json; charset=UTF-8")
                w.WriteHeader(422) // unprocessable entity
                if err := json.NewEncoder(w).Encode(err); err != nil {
                    panic(err)
                }
                return
            } 
            w.Header().Set("Content-Type", "application/json; charset=UTF-8")
            w.WriteHeader(http.StatusForbidden) //<---- viene per questo
            if err := json.NewEncoder(w).Encode(etcdError); err != nil {
                panic(err)
            }
            return
        } else {
            if err := json.Unmarshal(bodyPut, &etcdSet); err != nil {
                w.Header().Set("Content-Type", "application/json; charset=UTF-8")
                w.WriteHeader(422) // unprocessable entity
                if err := json.NewEncoder(w).Encode(err); err != nil {
                    panic(err)
                }
                return
            }
            w.Header().Set("Content-Type", "application/json; charset=UTF-8")
            w.WriteHeader(http.StatusOK)
            if err := json.NewEncoder(w).Encode(etcdSet); err != nil {
                panic(err)
            }
            return
        } 

    }

    /**
     * Funzione che viene invocata per richiedere la segnalazione salvata in DynamoDB
     */

    func GetSignalationFromDynamoDB(w http.ResponseWriter, r *http.Request){

         //parlare con DynamoDB
        vars := mux.Vars(r)
        long := vars["long"]
        lat := vars["lat"]

        input := &dynamodb.GetItemInput{
            Key: map[string]*dynamodb.AttributeValue{
                "longitude": {
                        N: aws.String(long),
                },
                "latitude":{
                        N: aws.String(lat),
                },
            },
            TableName: aws.String(dynamoDBTable),
        }

    
        result, err := svcDynamoDB.GetItem(input)
        if(err!=nil){
            panic(err)
        }
       // fmt.Fprintln(w, result)
        s := SignalationDb{}
        dynamodbattribute.UnmarshalMap(result.Item, &s)
        if err := json.NewEncoder(w).Encode(s); err != nil {
            panic(err)
        }
        w.Header().Set("Content-Type", "application/json; charset=UTF-8")
        w.WriteHeader(http.StatusOK)
    }

    /**
     * Funzione che viene invocata da un operatore web per richiedere quali sono i possibili stati che una segnalazione può assumere
     */

    func GetAllPossibleSignalationStates(w http.ResponseWriter, r *http.Request){

        initialize_state()
        w.Header().Set("Content-Type", "application/json; charset=UTF-8")
        w.WriteHeader(http.StatusOK)
        if err := json.NewEncoder(w).Encode(states); err != nil {
            panic(err)
        }

    }

    /**
     * Funzione che viene invocata da un operatore web per aggiornare lo stato di una segnalazione
     */

    func ChangeSignalationState(w http.ResponseWriter, r *http.Request){
        vars := mux.Vars(r)
        state := vars["state"]
        dir := vars["dir"]
        stateId := vars["stateId"]

        var etcdGetOneValue EtcdGetOneValue;
        var etcdSet EtcdSet;
        var etcdError EtcdError;
        
        respGet, err := http.Get(etcdDomaninKeys+dir+"/"+stateId)
         if err != nil {
            // handle error
            panic(err)
        }
        bodyGet, err := ioutil.ReadAll(respGet.Body)
        if err := json.Unmarshal(bodyGet, &etcdGetOneValue); err != nil {
            w.Header().Set("Content-Type", "application/json; charset=UTF-8")
            w.WriteHeader(422) // unprocessable entity
            if err := json.NewEncoder(w).Encode(err); err != nil {
                panic(err)
            }
        }
        value := etcdGetOneValue.NodeGetOneValue.Value
        words := strings.Split(value, "|")
        long := words[1]
        lat :=  words[2]
        newValue := state+"|"+ long+ "|" + lat

        reqPut, err := http.NewRequest("PUT", etcdDomaninKeys +dir + "/" + stateId+"?value="+newValue, nil)
        if err != nil {
            panic(err)
        }


        client := &http.Client{}
        respPut, err := client.Do(reqPut)
        if err != nil {
            panic(err)
        }
        
        bodyPut, err := ioutil.ReadAll(respPut.Body)
        if(respPut.StatusCode != http.StatusOK && respPut.StatusCode != http.StatusCreated) {
            if err := json.Unmarshal(bodyPut, &etcdError); err != nil {
                //dovrei a questo punto, siccome fallisce la put, ma la delete è stata eseguita, ripristinare il valore precedente prima
                //di ritornare..
                w.Header().Set("Content-Type", "application/json; charset=UTF-8")
                w.WriteHeader(422) // unprocessable entity
                if err := json.NewEncoder(w).Encode(err); err != nil {
                    panic(err)
                }
            } 
            w.Header().Set("Content-Type", "application/json; charset=UTF-8")
            w.WriteHeader(http.StatusForbidden)
            if err := json.NewEncoder(w).Encode(etcdError); err != nil {
                panic(err)
            }
        } else {
            if err := json.Unmarshal(bodyPut, &etcdSet); err != nil {
                w.Header().Set("Content-Type", "application/json; charset=UTF-8")
                w.WriteHeader(422) // unprocessable entity
                if err := json.NewEncoder(w).Encode(err); err != nil {
                    panic(err)
                }
            }
            w.Header().Set("Content-Type", "application/json; charset=UTF-8")
            w.WriteHeader(http.StatusOK)
            if err := json.NewEncoder(w).Encode(etcdSet); err != nil {
                panic(err)
            }
        } 

    }
    //(VA TESTATA)
    /**
     * Funzione che viene invocata da un operatore web per completare una segnalazione
     */
  
    func CompletedSignalation(w http.ResponseWriter, r *http.Request){
        //salvo nuova tabella di dynamoDB la segnalazione
        //ad esempio ci aggiungo altri campi, tipo:
          //parlare con DynamoDB
        var etcdSet EtcdSet
        var etcdError EtcdError
        vars := mux.Vars(r)
        long := vars["long"]
        lat := vars["lat"]
        dir := vars["dir"]
        keyId := vars["keyId"]
        
        input := &dynamodb.GetItemInput{
            Key: map[string]*dynamodb.AttributeValue{
                "longitude": {
                        N: aws.String(long),
                },
                "latitude":{
                        N: aws.String(lat),
                },
            },
            TableName: aws.String(dynamoDBTable),
        }

    
        resultGet, err := svcDynamoDB.GetItem(input)
        if(err!=nil){
            panic(err)
        }
       
       // fmt.Fprintln(w, result)
        s := SignalationDb{}
        dynamodbattribute.UnmarshalMap(resultGet.Item, &s)
        /*if err := json.NewEncoder(w).Encode(s); err != nil {
            panic(err)
        }*/
        var completedDBSingnalation CompletedDBSingnalation 
        completedDBSingnalation.Longitude =  long
        completedDBSingnalation.Latitude = lat
        completedDBSingnalation.Topic = s.Topic
        completedDBSingnalation.Title = s.Title
        completedDBSingnalation.Message = s.Message
        completedDBSingnalation.Image = s.Image
        completedDBSingnalation.Operator = dir //questo me lo faccio passare, in realtà è s.Owner
        completedDBSingnalation.By = s.By
        completedDBSingnalation.Due = s.Due
        completedDBSingnalation.Completed = time.Now().Format(time.RFC850)
    
   
         //parlare con DynamoDB
        
        _, err = svcDynamoDB.PutItem(&dynamodb.PutItemInput{
            TableName: aws.String( dynamoDBTableRepository), 
            Item: map[string]*dynamodb.AttributeValue{ 
                "longitude": {
                        N: aws.String(completedDBSingnalation.Longitude),
                },
                "latitude":{
                        N: aws.String(completedDBSingnalation.Latitude),
                },
                "topic":{
                        S: aws.String(completedDBSingnalation.Topic),
                },
                "title":{
                        S: aws.String(completedDBSingnalation.Title),
                },
                "message":{
                        S: aws.String(completedDBSingnalation.Message),
                },
                "image":{
                        S: aws.String(completedDBSingnalation.Image),
                },
                "operator":{
                        S: aws.String(completedDBSingnalation.Operator),
                },
                "by":{
                        S: aws.String(completedDBSingnalation.By),
                },
                "created":{
                        S: aws.String(completedDBSingnalation.Due),
                },
                "completed":{
                        S: aws.String(completedDBSingnalation.Completed),
                },
            },
        })
        if(err!=nil){
            panic(err)
        }
        
        //se è andato a buon fine, cancello dalla tabella il record
        
        _, err = svcDynamoDB.DeleteItem(&dynamodb.DeleteItemInput{
            TableName: aws.String(dynamoDBTable), 
            Key:  map[string]*dynamodb.AttributeValue{
                "longitude": {
                        N: aws.String(long),
                    },
                "latitude":{
                        N: aws.String(lat),
                },
            },
        })
        if(err!=nil){
            panic(err)
        }

         
        //cancello da etcd lo stato
        
        req, err := http.NewRequest("DELETE", etcdDomaninKeys+dir + "/" + keyId, nil)
        if err != nil {
            log.Fatalln(err)
        }

        client := &http.Client{}
        respDel, err := client.Do(req)
        if err != nil {
            log.Fatalln(err)
        }

        bodyDel, err := ioutil.ReadAll(respDel.Body)

        //ritorno credo il json della nuova tabella, ma anche no content va bene

        if(respDel.StatusCode != http.StatusOK && respDel.StatusCode != http.StatusCreated) {
            if err := json.Unmarshal(bodyDel, &etcdError); err != nil {
                //dovrei a questo punto, siccome fallisce la put, ma la delete è stata eseguita, ripristinare il valore precedente prima
                //di ritornare..
                w.Header().Set("Content-Type", "application/json; charset=UTF-8")
                w.WriteHeader(422) // unprocessable entity
                if err := json.NewEncoder(w).Encode(err); err != nil {
                    panic(err)
                }
                return
            } 
            w.Header().Set("Content-Type", "application/json; charset=UTF-8")
            w.WriteHeader(http.StatusForbidden)
            if err := json.NewEncoder(w).Encode(etcdError); err != nil {
                panic(err)
            }
            return
        } else {
            if err := json.Unmarshal(bodyDel, &etcdSet); err != nil {
                w.Header().Set("Content-Type", "application/json; charset=UTF-8")
                w.WriteHeader(422) // unprocessable entity
                if err := json.NewEncoder(w).Encode(err); err != nil {
                    panic(err)
                }
                return
            }
            w.Header().Set("Content-Type", "application/json; charset=UTF-8")
            w.WriteHeader(http.StatusOK)
            if err := json.NewEncoder(w).Encode(etcdSet); err != nil {
                panic(err)
            }
            return
        } 
    }