package main

type SignalationScans struct {
    Count   int  		 `json:"Count"`
	Items   SignalationsDb `json:"Items"`
	ScannedCount int 	 `json:"ScannedCount"`
}

type SignalationDb struct {
    Topic     string    `json:"topic"`
    Title     string    `json:"title"`
    Message   string    `json:"message"`
    Longitude float64   `json:"longitude"`
    Latitude  float64   `json:"latitude"`
    Image	  string	`json:"image"`
    Due       string 	`json:"created"`
    StateId   string    `json:"stateId"`
    Owner     string    `json:"owner"`
    By        string    `json:"by"`
}

type SignalationsDb []SignalationDb

type SignalationToAndroid struct {
    Topic     string    `json:"topic"`
    Title     string    `json:"title"`
    Message   string    `json:"message"`
    Longitude float64   `json:"longitude"`
    Latitude  float64   `json:"latitude"`
    Image     string    `json:"url"`
    Due       string    `json:"due"`
    StateId   string    `json:"stateId"`
    Owner     string    `json:"owner"`
    By        string    `json:"by"`
}

type SignalationsToAndroid []SignalationToAndroid