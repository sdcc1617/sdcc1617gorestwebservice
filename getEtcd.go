package main

type EtcdGet struct {
    Action    string    `json:"action"`
	NodeGet NodeGet 	`json:"node"`
}

type NodeGet struct {
	Key    		string  	`json:"key"`
	Dir 		bool 		`json:"dir"`
	Nodes 		Nodes 		`json:"nodes"`
	ModifiedIndex   int  	`json:"modifiedIndex"`
	CreatedIndex    int  	`json:"createdIndex"`
}

type EtcdGetOneValue struct {
	Action    string  				`json:"action"`
	NodeGetOneValue NodeGetOneValue `json:"node"`	
}

type NodeGetOneValue struct {
	Key    		string  	`json:"key"`
	Value 		string 		`json:"value"`
	ModifiedIndex   int  	`json:"modifiedIndex"`
	CreatedIndex    int  	`json:"createdIndex"`
}