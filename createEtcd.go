package main

type EtcdCreate struct {
    Action    string  `json:"action"`
	NodeCreate NodeCreate `json:"node"`
}

type NodeCreate struct {
	Key    			string  	`json:"key"`
	Value    		string  	`json:"value"`
	ModifiedIndex   int  		`json:"modifiedIndex"`
	CreatedIndex    int  		`json:"createdIndex"`
}