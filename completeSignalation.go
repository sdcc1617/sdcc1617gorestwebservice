package main

type BodyComplete struct {
    Operator  string  `json:"operator"`
    Completed string  `json:"completed"`
}

type CompletedDBSingnalation struct {

	Longitude string    `json:"longitude"`
    Latitude  string    `json:"latitude"`
    Topic     string    `json:"topic"`
    Title     string    `json:"title"`
    Message   string    `json:"message"`
    Image	  string	`json:"url"`
	Operator  string    `json:"operator"`
    By        string    `json:"by"`
	Due       string 	`json:"due"`
    Completed string    `json:"completed"`
}