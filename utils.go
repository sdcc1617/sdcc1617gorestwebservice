package main
import(
	"math/rand"
	"time"
)
/**
 * Funzioni per generare stringhe in maniera casuale
 * 
 */
 
func init() {
    rand.Seed(time.Now().UnixNano())
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
var numberRunes = []rune("1234567890")
var letterNumberRunes = []rune("1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func RandStringRunes(n int) string {
    b := make([]rune, n)
    for i := range b {
        b[i] = letterRunes[rand.Intn(len(letterRunes))]
    }
    return string(b)
}

func RandNumberRunes(n int) string {
    b := make([]rune, n)
    for i := range b {
        b[i] = numberRunes[rand.Intn(len(numberRunes))]
    }
    return string(b)
}

func RandLetterNumberRunes(n int) string {
    b := make([]rune, n)
    for i := range b {
        b[i] = letterNumberRunes[rand.Intn(len(letterNumberRunes))]
    }
    return string(b)
}