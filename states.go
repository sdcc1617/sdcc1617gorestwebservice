package main

type State struct {
	Value  string  	`json:"value"`
}
type States [3]State

var states States
func initialize_state(){
	states[0].Value = "TODO"
	states[1].Value = "IN_PROGRESS"
	states[2].Value = "DONE"
}
