package main

type EtcdSet struct {
    Action    string  `json:"action"`
    Node      Object  `json:"node"`
    PrevNode  Object  `json:"prevNode"`
}

type EtcdError struct {
	ErrorCode int `json:"errorCode"`
  	Message string`json:"message"`
 	Cause string `json:"cause"`
    Index int `json:"index"`
}